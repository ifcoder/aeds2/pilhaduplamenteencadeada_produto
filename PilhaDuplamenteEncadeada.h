#ifndef DUPLAMENTEENCADEADA_H
#define	DUPLAMENTEENCADEADA_H

#include "Produto.h"
#include "Nodo.h"

class PilhaDuplamenteEncadeada {
private:
    int quant;
    Nodo *head;

    bool isEmpty (); 
    Nodo* getElemento(int posicao);
    
    
public:
    PilhaDuplamenteEncadeada();
    virtual ~PilhaDuplamenteEncadeada();
    
    void insert();         
    void remove();                 
        
    void imprimir();
    void preencher();                
    
    //GETTERS E SETTERS
    void setQuant(int quant);
    int getQuant() const;
};

#endif	

