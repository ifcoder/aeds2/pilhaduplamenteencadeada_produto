# Pilha Duplamente Encadeada (Produto)

- Este código C++ pertence a disciplina Algoritmos e Estrutura de Dados II
- Esta é a segunda estrutura de dados que vamos estudar
- [Material de estudo](https://summer-pocket-6a4.notion.site/2-3-Lista-Duplamente-Encadeada-98017486fc99480eb2cea24e867455bf) 

## PILHA Duplamente Encadeada
- A PILHA duplamente encadeada tem como base a lista duplamente encadeada
- A novidade aqui é a regra: FILO (First In, Last Out)
- Ou seja:
    - a inserção será no INICIO da FILA
    - a remoção no INICIO da Fila
[] O interessante é que isso não implica em alteração alguma na lista duplamente encadeada, visto que a lista também insere e remove na primeira posicao
[] Basicamente o que muda é o nome dos metodos e alguns métodos deixam de existir, como é o caso do buscar e getElemento
[] Estes casos devem ser tratados com bastante atenção
  
## Linguagem C++
- O estudo desta disciplina é feito usando a linguagem de programação C++

## NetBeans
- Este projeto foi desenvolvido utilizando a IDE Netbeans. 
- Para rodá-lo é necessário ter o compilador C++ em sua máquina e a IDE NetBeans configurada para a linguagem C++

