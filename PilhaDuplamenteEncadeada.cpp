#include "PilhaDuplamenteEncadeada.h"
#include<iostream>

using namespace std;

PilhaDuplamenteEncadeada::PilhaDuplamenteEncadeada() {
    quant = 0;
    head = NULL;
}

PilhaDuplamenteEncadeada::~PilhaDuplamenteEncadeada() {
    //será chamado quando o objeto for destruido
}

/**
 * Retorna o endereco do elemento da posiçao: n
 * @param posicao
 * @return 
 */
Nodo* PilhaDuplamenteEncadeada::getElemento(int n) {
    Nodo* p = head;
    int j = 1;

    while (j <= n - 1 && p->getProx() != NULL) {
        p = p->getProx();
        j++;
    }

    if (j == n)
        return p;
    else
        return NULL; // posicao invalida

}

void PilhaDuplamenteEncadeada::preencher() {
    int q = 0;
    do{
        cout << "Quantos elementos voce deseja inserir:";
        cin >> q;
    }while(q < 0);
    
    for(int i=0; i<= q-1 ;i++){
        this->insert();
    }
}

void PilhaDuplamenteEncadeada::insert() {
    Produto p;
    p.preencher();
    Nodo *novo;
    novo = new Nodo(p);
    
    if(quant == 0){ //Fila está vazia
        head = novo;
    }else{
        novo->setProx(head);
        head->setAnt(novo);
        head= novo;
    }
    
    quant++;
}


void PilhaDuplamenteEncadeada::remove() {
    if(quant > 0){
        if(quant == 1){
            head = head->getProx();
            //não tem segundo elemento para fazer o apontamento para trás
        }else { //lista tem mais de um elemento
            head = head->getProx();
            head->setAnt(NULL);
        }
        quant--;
    }else{
        cout << "Operacao invalida - Lista vazia";
    }
}

bool PilhaDuplamenteEncadeada::isEmpty() {
    if (quant == 0)
        return true;
    else
        return false;

}

void PilhaDuplamenteEncadeada::imprimir() {
    cout << "\n>> Lista [ ";
    if (!isEmpty()) {
        Nodo* p = head;
        while (p != NULL) {
            p->getItem().imprimirResumido();
            p = p->getProx();
        }
    }
    cout << " ] \n";
}

/**
 *  GETTERS E SETTERS 
 */
void PilhaDuplamenteEncadeada::setQuant(int quant) {
    this->quant = quant;
}

int PilhaDuplamenteEncadeada::getQuant() const {
    return quant;
}
